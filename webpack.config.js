const path = require('path');
const fs = require('fs');
const isProduction = process.env.NODE_ENV == 'production';
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const NunjucksWebpackPlugin = require('nunjucks-webpack-plugin');
const nunjuckspages = require('./nunjuckspages');

let fileList = [];
fileList = fs.readdirSync('./src/images').filter((file) => file.match(/.+\.(png|jpg|gif)/)).map((file) => './images/' + file);
fileList.push('./src/index.js');

module.exports = {
	// devtool: 'eval-cheap-module-source-map',
	devtool: 'eval-source-map',
	mode: 'development',
	entry: {
		main: fileList,
	},
	output: {
		filename: isProduction ? '[name]-[contenthash].app.js' : '[name].app.js',
		path: path.resolve(__dirname, 'assets')
	},
	devServer: {
		port: 8080,
		contentBase: path.join(__dirname, "dist")
	},
	node: {
		fs: 'empty'
	},
	module: {
		rules: [
			{
				test: /\.(jpg|png|gif|svg)$/,
				loader: 'image-webpack-loader',
				// Specify enforce: 'pre' to apply the loader
				// before url-loader/svg-url-loader
				// and not duplicate it in rules with them
				enforce: 'pre'
			},
			{
				test: /\.font\.js$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					{
						loader: 'webfonts-loader',
						options: {
							publicPath: '/assets/'
						}
					}
				]
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				loader: 'babel-loader',
				options: {
					presets: ['env']
				}
			},
			{
				test: /\.(sa|sc|c)ss$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					'sass-loader'
				]
			},
			{
				// Load all images as base64 encoding if they are smaller than 8192 bytes
				test: /\.(png|jpg|gif)$/,
				use: [
					{
						loader: 'url-loader',
						options: {
							// On development we want to see where the file is coming from, hence we preserve the [path]
							name: '[path][name].[ext]?hash=[hash:20]',
							limit: 8192
						}
					}
				]
			},
			{
				test: /\.svg/,
				use: {
					loader: 'svg-url-loader',
					options: {}
				}
			}
		],
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: isProduction ? '[name]-[contenthash].css' : '[name].css',
			chunkFilename: '[id].css'
		}),
    new NunjucksWebpackPlugin({
       templates: nunjuckspages
     })
	]
};
