module.exports = {
	'files': [
		'./fonts/icons/*.svg'
	],
	'fontName': 'sngfonticons',
	'classPrefix': 'icon-',
	'baseSelector': '.icon',
	'types': ['eot', 'woff', 'woff2', 'ttf', 'svg'],
	'fileName': 'app.[fontname].[ext]'
};
